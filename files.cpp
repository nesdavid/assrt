#if USE_STB
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#endif

enum image_format
{
    image_format_BMP,
    image_format_PNG,
    image_format_JPG,
    image_format_PPM
};

void write_image(char const* path, image_buffer const& buffer, image_format format)
{
    SCOPED_TIMER(write_image);
#if !USE_STB
    printf("Only PPM format supported in this build\n");
    format = image_format_PPM;
#endif

    char* image_name = new char[strlen(path) + 5];

    if (format == image_format_PPM)
    {
        std::ofstream outdata;

        sprintf(image_name, "%s%s", path, ".ppm");
        outdata.open(image_name);

        if (!outdata)
        {
            std::cerr << "Error: file could not be opened" << std::endl;
            exit(1);
        }

        outdata << "P3" << std::endl;
        outdata << buffer.width << " " << buffer.height << std::endl;
        outdata << "255" << std::endl;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j][i], buffer.samples_per_pixel);
                outdata << (int)(pixel.r) << " "
                        << (int)(pixel.g) << " "
                        << (int)(pixel.b) << " ";
            }
            outdata << std::endl;
        }

        outdata.close();
    }
    else
    {
#if USE_STB
        int channels = 3;
        uint8_t* data = new uint8_t[buffer.height * buffer.width * channels];
        int index = 0;

        for (int j = buffer.height - 1; j >= 0; --j)
        {
            for (int i = 0; i < buffer.width; ++i)
            {
                bytes_color pixel(buffer.pixels[j][i], buffer.samples_per_pixel);

                for (int c = 0; c < channels; ++c)
                {
                    data[index++] = pixel.e[c];
                }
            }
        }

        if (format == image_format_PNG)
        {
            sprintf(image_name, "%s%s", path, ".png");
            stbi_write_png(image_name, buffer.width, buffer.height, channels, data, buffer.width * channels);
        }
        else if (format == image_format_JPG)
        {
            sprintf(image_name, "%s%s", path, ".jpg");
            stbi_write_jpg(image_name, buffer.width, buffer.height, channels, data, 100);
        }
        else
        {
            sprintf(image_name, "%s%s", path, ".bmp");
            stbi_write_bmp(image_name, buffer.width, buffer.height, channels, data);
        }

        delete [] data;
#endif
    }

    delete [] image_name;
}

