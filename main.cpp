#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory>
#include <thread>

#define USE_SDL 1
#define USE_STB 0
#define PERF_TIMERS 0 // this is not ok, 300ms render gave a 0.050s frame time...

int window_width = 480;
int window_height = 360;
int target_framerate = 60; // lolz
int raytrace_downscale = 1;
int samples_per_pixel = 16;
int max_depth = 8;

const int max_threads = 8;

#define CONCAT(a, b) CONCAT_INNER(a, b)
#define CONCAT_INNER(a, b) a ## b

#include "performance.cpp"
#include "math.cpp"
#include "render.cpp"
#include "scenes.cpp"

#if USE_SDL
#include <SDL2/SDL.h>
#else
#include "files.cpp"
#endif

enum pixel_job_status
{
    pixel_job_status_todo,
    pixel_job_status_wip,
    pixel_job_status_done,
};

struct pixel_job
{
    pixel_job() : status(pixel_job_status_done) { }
    int status;
    int i, j;
};

struct thread_setup
{
    int max_depth;
    image_buffer* image_data;
    camera* cam;
    hittable_list* world;
};

pixel_job pending_jobs[max_threads];
std::mutex job_pool_mutex;
bool process_jobs;

void queue_pixel_job(int i, int j)
{
    bool queued = false;

    while (!queued)
    {
        job_pool_mutex.lock();

        for (int t = 0; t < max_threads; ++t)
        {
            if (pending_jobs[t].status == pixel_job_status_done)
            {
                pending_jobs[t].i = i;
                pending_jobs[t].j = j;
                pending_jobs[t].status = pixel_job_status_todo;

                queued = true;
                process_jobs = true;
                break;
            }
        }

        job_pool_mutex.unlock();
    }
}

void wait_all_pixel_jobs()
{
    bool no_pending_jobs = true;

    while (!no_pending_jobs)
    {
        no_pending_jobs = false;

        job_pool_mutex.lock();

        for (int t = 0; t < max_threads; ++t)
        {
            if (pending_jobs[t].status != pixel_job_status_done)
            {
                no_pending_jobs = true;
                break;
            }
        }

        job_pool_mutex.unlock();
    }
}

bool start_pixel_job(pixel_job** job)
{
    (*job) = NULL;

    job_pool_mutex.lock();

    for (int i = 0; i < max_threads; ++i)
    {
        if (pending_jobs[i].status == pixel_job_status_todo)
        {
            *job = &pending_jobs[i];
            (*job)->status = pixel_job_status_wip;
            break;
        }
    }

    job_pool_mutex.unlock();

    return *job != NULL;
}

void pixel_worker(void* args)
{
    thread_setup* config = (thread_setup*)args;

    while (process_jobs)
    {
        pixel_job* current_job;

        if (start_pixel_job(&current_job))
        {
            color c = color::zero();

            for (int s = 0; s < config->image_data->samples_per_pixel; ++s)
            {
                float u = float(current_job->i + random_float()) / float(config->image_data->width-1);
                float v = float(current_job->j + random_float()) / float(config->image_data->height-1);
                ray r = config->cam->get_ray(u, v);
                c += ray_color(r, *config->world, config->max_depth);
            }

            config->image_data->pixels[current_job->j][current_job->i] = c;
            current_job->status = pixel_job_status_done;
        }
    }
}

std::unique_ptr<std::thread> threads_pool[max_threads];

void init_thread_pool(thread_setup* config)
{
    process_jobs = true;

    for (int i = 0; i < max_threads; ++i)
    {
        threads_pool[i] = std::make_unique<std::thread>(pixel_worker, config);
    }
}

void stop_thread_pool()
{
    process_jobs = false;

    for (int i = 0; i < max_threads; ++i)
    {
        threads_pool[i]->join();
        threads_pool[i] = nullptr;
    }
}

void raytrace_frame(image_buffer* image_data)
{
    SCOPED_TIMER(raytrace_image);

    for (int j = image_data->height-1; j >= 0; --j)
    {
        for (int i = 0; i < image_data->width; ++i)
        {
            queue_pixel_job(i, j);
        }
    }

    wait_all_pixel_jobs();
}

int main (int argc, char* argv[])
{

    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-w") == 0 && argc > i + 1 )
        {
            window_width = (int)strtol(argv[++i], NULL, 10);
        }
        else if (strcmp( argv[i], "-h") == 0 && argc > i + 1 )
        {
            window_height = (int)strtol(argv[++i], NULL, 10);
        }
        else if (strcmp( argv[i], "-fps") == 0 && argc > i + 1)
        {
            target_framerate = (int)strtol(argv[++i], NULL, 10);
        }
    }

    int frametime_target = TIMER_PRECISION / target_framerate;
    float aspect_ratio = (float)window_width/(float)window_height;

    hittable_list world;
    camera cam;

    build_scene(&cam, &world, aspect_ratio);

    image_buffer image_data(window_width/raytrace_downscale, window_height/raytrace_downscale, samples_per_pixel);

    thread_setup config = 
    {
        max_depth,
        &image_data,
        &cam,
        &world
    };

    init_thread_pool(&config);

#if USE_SDL
    /* There are no error checks for brevity.  If something goes wrong, it's easy to add a check and a call to SDL_GetError() to figure out why.  */ /* We initialize the SDL video and event subsystems.  If we forget or leave ot the SDL_INIT_EVENTS, the SDL_CreateWindow() will initialize it for us.  There is no error checking in our example code, and in a real game we might explicitly check if this fails and try to notify the user somehow.  */ SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

    /*
       We create a new window to display our message.  Here we pass in *
       the title text, where it should be on the screen (which is *
       undefined) and the size as width and height, and finally a flag to say *
       it's shown.
     */
    SDL_Window *window = SDL_CreateWindow("A Simple and Slow Raytracer | ASS RT", 
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, 
            window_width, window_height, SDL_WINDOW_SHOWN);

    SDL_SetWindowResizable(window, SDL_TRUE);

    SDL_Surface *surface = SDL_LoadBMP( "hello.bmp" );

    /*
       We create a hardware accelerated renderer.  This is liable to
       fail on some systems, and again there's no error checking.

       We pass in the window we created earlier, and use -1 for the
       rendering driver to get the first one available.

       We can explicitly ask for hardware redering with SDL_RENDERER_ACCELERATED.
     */
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
    SDL_RenderSetLogicalSize(renderer, image_data.width, image_data.height);

    // We create the texture we want to disaply from the surface we loaded earlier.
    SDL_Texture* bmp_texture = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_Texture* sdlTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, image_data.width, image_data.height);

    // We're now done with the surface, so we free the resources.
    // And set the pointer to NULL so it won't accidentally be used for something else.
    SDL_FreeSurface(surface);
    surface = NULL;

    SDL_bool quit = SDL_FALSE;
    SDL_Event e = { 0 };

    float dt = 0.0f;

    int channels = 4;
    uint8_t* data = new uint8_t[image_data.height * image_data.width * channels];

    /*
       Here is a very simple game loop, with a soft framerate.  There is
       a chance an individual frame will take longer, and this happens
       predictably when the user presses the X button to close the
       window, and the delay routine can oversleep because of operating
       system scheduling.
     */
    while (!quit)
    {
        Uint32 start = SDL_GetTicks();

        vec3 delta_cam(0, 0, 0);

        while (SDL_PollEvent(&e))
        {
            switch (e.type)
            {
                case SDL_QUIT:
                    quit = SDL_TRUE;
                    break;
                default:
                    break;
            }
        }

        const uint8_t* kb = SDL_GetKeyboardState(NULL);

        delta_cam.x = kb[SDL_SCANCODE_D] - kb[SDL_SCANCODE_A];
        delta_cam.z = kb[SDL_SCANCODE_S] - kb[SDL_SCANCODE_W];

        float delta_cam_len = delta_cam.length();

        if (delta_cam_len > 1.0f)
        {
            delta_cam = delta_cam / delta_cam_len;
        }

        cam.move(delta_cam * dt);

        raytrace_frame(&image_data);

        /*
           We clear the screan with the default colour (because we don't
           explicitly set the colour anywhere).
         */
        SDL_RenderClear(renderer);

        /*
           Then copy the texture we created on to the entire screen.
           That's what the NULL, NULL means: use the entire texture on the
           entire screen.
         */
        /*
           SDL_RenderCopy(renderer, bmp_texture, NULL, NULL );
         */

        {
            SCOPED_TIMER("float_to_byte");
            int index = 0;
            for (int y = image_data.height - 1; y >= 0; --y)
            {
                for (int x = 0; x < image_data.width; ++x)
                {

                    bytes_color pixel(image_data.pixels[y][x], image_data.samples_per_pixel);
                    data[index++] = pixel.b;
                    data[index++] = pixel.g;
                    data[index++] = pixel.r;
                    data[index++] = 0;
                }
            }
        }

        SDL_UpdateTexture(sdlTexture, NULL, data, image_data.width * channels * sizeof(uint8_t));

        SDL_RenderCopy(renderer, sdlTexture, NULL, NULL);

        // And finally tell the renderer to display on screen what we've drawn so far.
        SDL_RenderPresent(renderer);

        Uint32 end = SDL_GetTicks();

        int frame_ms = end - start;
        int wait_for = frametime_target - frame_ms;

        if (wait_for > 0)
        {
            SDL_Delay(wait_for);
        }

        dt = (float)(SDL_GetTicks() - start) / (float)TIMER_PRECISION;

        printf("\r > dt: %.3f | fps: %.2f\r", dt, 1.0f/dt);
        fflush(NULL);
    }

    delete [] data;

    // Clean up after ourselves before we quit.
    SDL_DestroyTexture(bmp_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
#else
    raytrace_frame(&image_data);
    write_image("image", image_data, image_format_BMP);
#endif

    stop_thread_pool();

    printf("\n");

    return 0;
}

