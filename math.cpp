#include <limits>
#include <math.h>
#include <random>
#include <stdint.h>

#define SIMD_VEC 0

// Constants
float infinity = std::numeric_limits<float>::infinity();
float pi = 3.1415926535897932385f;

// Utility Functions
inline float random_float()
{
#if 0
    static std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
    static std::mt19937 generator;
    return distribution(generator);
#else
    // goodol' c random is faster...
    return rand() / (RAND_MAX + 1.0);
#endif
}

inline float random_float(float min, float max)
{
    return min + (max-min) * random_float();
}

inline float degrees_to_radians(float degrees)
{
    return degrees * pi / 180.0f;
}

inline float clamp(float value, float lower, float upper)
{
    return value < lower ? lower : (value > upper ? upper : value);
}

union vec3
{
    struct {
        float x, y, z;
    };
    struct {
        float r, g, b;
    };
    float e[3];

#if SIMD_VEC
    __m128 m;
#endif

    static vec3 one()
    {
        return vec3(1, 1, 1);
    }

    static vec3 zero()
    {
        return vec3(0, 0, 0);
    }

    static vec3 random()
    {
        return vec3(random_float(), random_float(), random_float());
    }

    static vec3 random(float min, float max)
    {
        return vec3(random_float(min, max), random_float(min, max), random_float(min, max));
    }

    vec3() { }

    vec3(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    vec3(vec3 const& other)
    {
#if SIMD_VEC
        m = _mm_load_ps(other.e);
#else
        this->x = other.x;
        this->y = other.y;
        this->z = other.z;
#endif
    }

#if SIMD_VEC
    vec3(__m128 const& other_m)
    {
        m = other_m;
    }
#endif

    float length() const
    {
        return sqrt(length_squared());
    }

    float length_squared() const
    {
        return x*x + y*y + z*z; 
    }

    char* c_str(char* buffer) const
    {
        sprintf(buffer, "(%f, %f, %f)", x, y, z);
        return buffer;
    }

    inline vec3& operator+=(vec3 const& other)
    {
#if SIMD_VEC
        m = _mm_add_ps(m, other.m);
#else
        x += other.x;
        y += other.y;
        z += other.z;
#endif
        return *this;
    }

    inline vec3& operator-=(vec3 const& other)
    {
#if SIMD_VEC
        m = _mm_sub_ps(m, other.m);
#else
        x -= other.x;
        y -= other.y;
        z -= other.z;
#endif
        return *this;
    }

    inline bool near_zero()
    {
        const auto s = 1e-8;
        return (fabs(x) < s) && (fabs(y) < s) && (fabs(z) < s);
    }
};

typedef vec3 color;

inline vec3 operator+(vec3 const& a, vec3 const& b)
{
#if SIMD_VEC
    __m128 result = _mm_add_ps(a.m, b.m);
    return vec3(result);
#else
    return vec3(a.x + b.x, a.y + b.y, a.z + b.z);
#endif
}

inline vec3 operator-(vec3 const& v)
{
    return vec3(-v.x, -v.y, -v.z);
}

inline vec3 operator-(vec3 const& a, vec3 const& b)
{
#if SIMD_VEC
    __m128 result = _mm_sub_ps(a.m, b.m);
    return vec3(result);
#else
    return vec3(a.x - b.x, a.y - b.y, a.z - b.z);
#endif
}

inline vec3 operator*(vec3 const& a, vec3 const& b)
{
#if SIMD_VEC
    __m128 result = _mm_mul_ps(a.m, b.m);
    return vec3(result);
#else
    return vec3(a.x * b.x, a.y * b.y, a.z * b.z);
#endif
}

inline vec3 operator*(vec3 const& v, float s)
{
#if SIMD_VEC
    __m128 s_m = _mm_load_ps1(&s);
    __m128 result = _mm_mul_ps(v.m, s_m);
    return vec3(result);
#else
    return vec3(v.x * s, v.y * s, v.z * s);
#endif
}

inline vec3 operator*(float s, vec3 const& v)
{
    return v * s;
}

inline vec3 operator/(vec3 const& v, float s)
{
#if SIMD_VEC
    __m128 s_m = _mm_load_ps1(&s);
    __m128 result = _mm_div_ps(v.m, s_m);
    return vec3(result);
#else
    return vec3(v.x / s, v.y / s, v.z / s);
#endif
}

inline float dot(vec3 const& u, vec3 const& v)
{
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

inline vec3 cross(vec3 const& u, vec3 const& v)
{
    return vec3(u.y * v.z - u.z * v.y,
                u.z * v.x - u.x * v.z,
                u.x * v.y - u.y * v.x);
}

inline vec3 unit_vector(vec3 const& v)
{
    return v / v.length();
}

inline vec3 random_in_unit_sphere()
{
    for (;;)
    {
        vec3 p = vec3::random(-1,1);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        return p;
    }
}

inline vec3 random_unit_vector()
{
    return unit_vector(random_in_unit_sphere());
}

inline vec3 random_in_hemisphere(vec3 const& normal)
{
    vec3 in_unit_sphere = random_in_unit_sphere();
    if (dot(in_unit_sphere, normal) > 0.0)
    {
        // In the same hemisphere as the normal
        return in_unit_sphere;
    }
    else
    {
        return -in_unit_sphere;
    }
}

inline vec3 reflect(const vec3& v, const vec3& n)
{
    return v - 2 * dot(v,n) * n;
}

vec3 refract(const vec3& uv, const vec3& n, double etai_over_etat)
{
    auto cos_theta = fmin(dot(-uv, n), 1.0);
    vec3 r_out_perp =  etai_over_etat * (uv + cos_theta*n);
    vec3 r_out_parallel = -sqrt(fabs(1.0 - r_out_perp.length_squared())) * n;
    return r_out_perp + r_out_parallel;
}

vec3 random_in_unit_disk()
{
    for (;;)
    {
        vec3 p = vec3(random_float(-1.0f, 1.0f), random_float(-1.0f, 1.0f), 0.0f);

        if (p.length_squared() >= 1)
        {
            continue;
        }

        return p;
    }
}

