#include <time.h>

#define TIMER_PRECISION 1000

#if PERF_TIMERS
#define SCOPED_TIMER(label) ScopedTimer CONCAT(timer, __COUNTER__)(#label)

struct ScopedTimer
{
    ScopedTimer(std::string const& label)
    : label(label), start(clock()) {}

    ~ScopedTimer()
    {
        printf("ScopedTimer %s: %ldms\n", label.c_str(), (clock()-start)* TIMER_PRECISION / CLOCKS_PER_SEC);
    }

    std::string label;
    clock_t start;
};
#else
#define SCOPED_TIMER(label)
#endif

#if PERF_TIMERS
#define FRAME_TIMER_INIT(label) FrameTimer::Init(label)
#define FRAME_TIMER_SHOW(label) FrameTimer::Show(label)
#define FRAME_TIMER(label) FrameTimer timer(label)

#include <unordered_map>

struct FrameTimer
{
    static std::unordered_map<std::string, clock_t> timers;

    static void Init(std::string const& label)
    {
        timers[label] = 0;
    }

    static void Show(std::string const& label)
    {
        printf("FrameTimer %s: %ldms\n", label.c_str(), timers[label]);
    }


    FrameTimer(std::string const& label)
    : start(clock()), counter(&timers[label])  {}

    ~FrameTimer()
    {
        *counter += (clock()-start)* TIMER_PRECISION / CLOCKS_PER_SEC;
    }

    clock_t start;
    clock_t* counter;
};

std::unordered_map<std::string, clock_t> FrameTimer::timers;

#else
#define FRAME_TIMER_INIT(label) 
#define FRAME_TIMER_SHOW(label)
#define FRAME_TIMER(label)
#endif

