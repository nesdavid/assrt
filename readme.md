**Huh!?**

After watching a [code review video](https://www.youtube.com/watch?v=mOSirVeP5lo) of a CPU offline raytrace renderer based on the [Ray Tracing in One Weekend](https://raytracing.github.io/) book series and I couldn't resist giving it a try.
The only real objective of this project is to learn something new and code something fun, but I'd love to achieve some level "real time" functionality even if it's at a really low resolution and try to scale it back up with some fancy technics. I'm only aiming at most for a cinematic 24fps scenario, if anything.

**Only tested on Mac because that's what I have in hand**

Windows and Linux support will likely come at some point (and Mac support will be likely dropped)
No clear structure, everything is a mess at the moment.

**ToDo** In no particular order:

- Refactor because it's horrible and it's a mess
- Lights
- Fully interactive camera
- SIMD, SoA and other CPU optimizations
- Run in GPU
- Cubes/boxes
- Meshes
- Textures
- Make a repo so you stop breaking things and having to guess how to fix them
- Scene management, "gameplay", FUN!

**Done** but I'm not happy with

- Display in a window
    I'd like to use a different solution, SDL is a bit of a black box and I'd like to properly move this
    to OpenGL or Vulkan to be able to use dear imgui and maybe AMD FSR or something like that

