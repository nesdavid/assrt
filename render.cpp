struct image_buffer
{
    image_buffer(int width, int height, int samples_per_pixel)
    {
        this->width = width;
        this->height = height;
        this->samples_per_pixel = samples_per_pixel;

        pixels = new color*[height];

        for (int j = 0; j < height; ++j)
        {
            pixels[j] = new color[width];
        }
    }

    int width;
    int height;
    int samples_per_pixel;
    color** pixels;
};

union bytes_color
{
    struct
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };

    uint8_t e[3];

    bytes_color(color color, int samples_per_pixel)
    {
        // Divide the color by the number of samples and gamma-correct for gamma=2.0.
        float scale = 1.0 / samples_per_pixel;
        r = (uint8_t)(clamp(sqrt(scale * color.r), 0.0f, 0.999f) * 256);
        g = (uint8_t)(clamp(sqrt(scale * color.g), 0.0f, 0.999f) * 256);
        b = (uint8_t)(clamp(sqrt(scale * color.b), 0.0f, 0.999f) * 256);
    }
};

struct ray
{
    vec3 origin;
    vec3 direction;

    ray()
    { }

    ray(vec3 const& origin, vec3 const& direction)
        : origin(origin), direction(direction)
    { }

    vec3 at(float t) const
    {
        return origin + direction * t;
    }
};

struct material;

struct hit_record
{
    vec3 p;
    vec3 normal;
    float t;
    bool front_face;
    material* mat;

    hit_record() : t(-1), front_face(false)
    { }

    void set_face_normal(ray r, vec3 outward_normal)
    {
        front_face = dot(r.direction, outward_normal) < 0;
        normal = front_face ? outward_normal : -outward_normal;
    }
};

struct material
{
    virtual bool scatter(ray const& r_in, hit_record const& hit, color* attenuation, ray* scattered) const = 0;
};

struct lambertian : material
{
    color albedo;

    lambertian(color const& a) : albedo(a) {}

    virtual bool scatter( const ray& r_in, const hit_record& hit, color* attenuation, ray* scattered) const override
    {
        // NOTE:
        // Other scattering tecnics for mate lambertian materials:
        // vec3 target = hit.p + random_in_hemisphere(hit.normal);
        // using random_in_hemisphere or random_in_unit_sphere will work, but 
        // it's not a proper aproximation and results in darker, less acurate results
        // It should be faster however because we're saving lenght and other
        // calculations.

        vec3 scatter_direction = hit.normal + random_unit_vector();

        if (scatter_direction.near_zero())
        {
            scatter_direction = hit.normal;
        }

        *scattered = ray(hit.p, scatter_direction);
        *attenuation = albedo;

        return true;
    }
};

struct metal : material
{
    color albedo;
    float fuzz;

    metal(color const& a, float f) : albedo(a), fuzz(clamp(f, 0, 1)) {}

    virtual bool scatter( const ray& r_in, const hit_record& hit, color* attenuation, ray* scattered) const override
    {
        vec3 reflected = reflect(unit_vector(r_in.direction), hit.normal);
        *scattered = ray(hit.p, reflected + fuzz*random_in_unit_sphere());
        *attenuation = albedo;
        return (dot(scattered->direction, hit.normal) > 0);
    }
};

struct dielectric : material
{
    float ir; // Index of Refraction

    dielectric(float index_of_refraction) : ir(index_of_refraction) {}

    virtual bool scatter(const ray& r_in, const hit_record& hit, color* attenuation, ray* scattered) const override
    {
        *attenuation = color(1.0f, 1.0f, 1.0f);

        float refraction_ratio = hit.front_face ? (1.0f/ir) : ir;

        vec3 unit_direction = unit_vector(r_in.direction);

        float cos_theta = fmin(dot(-unit_direction, hit.normal), 1.0f);
        float sin_theta = sqrt(1.0f - cos_theta*cos_theta);

        bool cannot_refract = refraction_ratio * sin_theta > 1.0f;
        vec3 direction;

        if (cannot_refract || reflectance(cos_theta, refraction_ratio) > random_float())
        {
            direction = reflect(unit_direction, hit.normal);
        }
        else
        {
            direction = refract(unit_direction, hit.normal, refraction_ratio);
        }

        *scattered = ray(hit.p, direction);

        return true;
    }

      static float reflectance(float cosine, float ref_idx)
      {
          // Use Schlick's approximation for reflectance.
          float r0 = (1.0f-ref_idx) / (1.0f+ref_idx);
          r0 = r0*r0;
          return r0 + (1.0f-r0) * pow((1.0f - cosine), 5.0f);
        }
};

struct hittable
{
    virtual hit_record hit(ray const& r, float t_min, float t_max) const = 0;
};

struct hittable_list : hittable
{
    std::vector<hittable*> list;

    hit_record hit(ray const& r, float t_min, float t_max) const
    {
        hit_record result;

        for (int i = 0; i < list.size(); ++i)
        {
            hittable* obj = list[i];
            hit_record test = obj->hit(r, t_min, t_max);

            if (test.t >= 0)
            {
                result = test;
                t_max = test.t;
            }
        }

        return result;
    }

    void add(hittable* object)
    {
        list.push_back(object);
    }
};

struct sphere : hittable
{
    vec3 center;
    float radius;
    material* mat;

    sphere(vec3 center, float radius, material* mat)
    : center(center), radius(radius), mat(mat)
    { }

    hit_record hit(ray const& r, float t_min, float t_max) const
    {
        hit_record result;

        vec3 oc = r.origin - center;
        float a = r.direction.length_squared();
        float half_b = dot(oc, r.direction);
        float c = oc.length_squared() - radius*radius;

        float discriminant = half_b*half_b - a*c;

        if (discriminant < 0)
        {
            return result;
        }

        float sqrtd = sqrt(discriminant);

// Find the nearest root that lies in the acceptable range.
        float root = (-half_b - sqrtd) / a;
        if (root < t_min or t_max < root)
        {
            root = (-half_b + sqrtd) / a;

            if (root < t_min or t_max < root)
            {
                return result;
            }
        }

        result.t = root;
        result.p = r.at(result.t);
        vec3 outward_normal = (result.p - center) / radius;
        result.set_face_normal(r, outward_normal);
        result.mat = mat;

        return result;
    }
};

color ray_color(ray const& original_ray, hittable_list const& world, int depth)
{
#if 0
    ray current_ray = original_ray;

    color* attenuation_list = new color[depth];

    int final_depth = 0;

    for (int i = 0; i < depth; ++i)
    {
        final_depth = i;

        if (i == depth-1)
        {
            attenuation_list[i] = color::zero();
        }
        else
        {
            hit_record hit = world.hit(current_ray, 0.001f, infinity);

            if (hit.t > 0.0f)
            {
                ray scattered;
                color attenuation;

                if (hit.mat->scatter(current_ray, hit, attenuation_list+i, &scattered))
                {
                    current_ray = scattered;
                }
                else
                {
                    attenuation_list[i] = color::zero();
                    break;
                }
            }
            else
            {
                vec3 unit_direction = unit_vector(current_ray.direction);
                float t = 0.5f * (unit_direction.y + 1.0f);
                attenuation_list[i] = ((1.0f-t) * color(1.0f, 1.0f, 1.0f) + t * color(0.5f, 0.7f, 1.0f));
                break;
            }
        }
    }

    color result_color = attenuation_list[final_depth];

    for (int i = final_depth-1; i >= 0 ; --i)
    {
        result_color = attenuation_list[i] * result_color;
    }

    delete [] attenuation_list;

    return result_color;
#else
    if (depth <= 0)
    {
        return color::zero();
    }

    hit_record hit = world.hit(original_ray, 0.001f, infinity);

    if (hit.t > 0.0f)
    {
        ray scattered;
        color attenuation;
        if (hit.mat->scatter(original_ray, hit, &attenuation, &scattered))
        {
            return attenuation * ray_color(scattered, world, depth-1);
        }
        else
        {
            return color::zero();
        }
    }

    vec3 unit_direction = unit_vector(original_ray.direction);
    float t = 0.5f * (unit_direction.y + 1.0f);
    return (1.0f-t) * color(1.0f, 1.0f, 1.0f) + t * color(0.5f, 0.7f, 1.0f);
#endif
}

struct camera
{
    vec3 origin;
    vec3 horizontal;
    vec3 vertical;
    vec3 lower_left_corner;
    vec3 w;
    vec3 u;
    vec3 v;
    float lens_radius;
    float focus_dist;

    camera() {}

    camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect_ratio, float aperture, float focus_dist)
    {
        float theta = degrees_to_radians(vfov);
        float h = tan(theta/2.0f);
        float viewport_height = 2.0f * h;
        float viewport_width = aspect_ratio * viewport_height;

        w = unit_vector(lookfrom - lookat);
        u = unit_vector(cross(vup, w));
        v = cross(w, u);

        horizontal = focus_dist * viewport_width * u;
        vertical = focus_dist * viewport_height * v;

        lens_radius = aperture / 2.0f;
        this->focus_dist = focus_dist;
    
        origin = vec3::zero();
        move(lookfrom);
    }

    void move(vec3 const& delta)
    {
        origin += delta;
        update_viewport();
    }

    void update_viewport()
    {
        lower_left_corner = origin - horizontal/2.0f - vertical/2.0f - focus_dist*w;
    }

    ray get_ray(float s, float t)
    {
        vec3 rd = lens_radius * random_in_unit_disk();
        vec3 offset = u * rd.x + v * rd.y;

        return ray(origin + offset, lower_left_corner + horizontal*s + vertical*t - origin - offset);
    }
};

