
#define SCENE 0

#define SCENE_SIMPLE 0
#define SCENE_EASY 1
#define SCENE_HARD 2


void random_scene(hittable_list* world, int amount)
{
    material* ground_material = new lambertian(color(0.5, 0.5, 0.5));
    world->add(new sphere(vec3(0,-1000,0), 1000, ground_material));

    for (int a = -amount; a < amount; a++) {
        for (int b = -amount; b < amount; b++) {
            float choose_mat = random_float();
            vec3 center(a + 0.9*random_float(), 0.2, b + 0.9*random_float());

            if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
                material* sphere_material;

                if (choose_mat < 0.8) {
                    // diffuse
                    color albedo = color::random() * color::random();
                    sphere_material = new lambertian(albedo);
                    world->add(new sphere(center, 0.2, sphere_material));
                } else if (choose_mat < 0.95) {
                    // metal
                    color albedo = color::random(0.5, 1);
                    float fuzz = random_float(0, 0.5);
                    sphere_material = new metal(albedo, fuzz);
                    world->add(new sphere(center, 0.2, sphere_material));
                } else {
                    // glass
                    sphere_material = new dielectric(1.5);
                    world->add(new sphere(center, 0.2, sphere_material));
                }
            }
        }
    }

    material* material1 = new dielectric(1.5);
    world->add(new sphere(vec3(0, 1, 0), 1.0, material1));

    material* material2 = new lambertian(color(0.4, 0.2, 0.1));
    world->add(new sphere(vec3(-4, 1, 0), 1.0, material2));

    material* material3 = new metal(color(0.7, 0.6, 0.5), 0.0);
    world->add(new sphere(vec3(4, 1, 0), 1.0, material3));
}

void build_scene(camera* cam, hittable_list* world, float aspect_ratio)
{
#if SCENE == SCENE_SIMPLE
    vec3 lookfrom(0,0,2);
    vec3 lookat(0,0,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 1;
    float aperture = 0.01f;
    float vfov = 45;

    lambertian* material_ground = new lambertian(color(0.8f, 0.8f, 0.0f));
    lambertian* material_center = new lambertian(color(0.1f, 0.2f, 0.5f));
    dielectric* material_left = new dielectric(1.5f);
    metal*      material_right = new metal(color(0.8f, 0.6f, 0.2f), 0.3f);

    world->add(new sphere(vec3( 0.0, -100.5, -1.0), 100.0, material_ground));
    world->add(new sphere(vec3( 0.0,    0.0, -1.0),   0.5, material_center));
    world->add(new sphere(vec3(-1.0,    0.0, -1.0),   0.5, material_left));
    world->add(new sphere(vec3(-1.0,    0.0, -1.0),   -0.4, material_left));
    world->add(new sphere(vec3( 1.0,    0.0, -1.0),   0.5, material_right));

#else
    vec3 lookfrom(-2,1,15);
    vec3 lookat(0,1,0);
    vec3 vup(0,1,0);
    float dist_to_focus = 10;
    float aperture = 0.1f;
    float vfov = 20;

#if SCENE == SCENE_EASY
    random_scene(&world, 6);
#elif SCENE == SCENE_HARD
    random_scene(&world, 11);
#endif
#endif

    *cam = camera(lookfrom, lookat, vup, vfov, aspect_ratio, aperture, dist_to_focus);
}

